// -*-c++-*-

// fixg2sxd - a utility to convert fig to sxd format

// Copyright (C) 2003-2022 Alexander Bürger, acfb@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef VECTOR2_H
#define VECTOR2_H

#include <cmath>

template<typename T>
class Vector2
{
    T x, y;
public:
    Vector2()
        : x( 0 ), y( 0 ) { }
    Vector2( T xx, T yy )
        : x( xx ), y( yy ) { }
    Vector2 operator+(Vector2 const& o) const
        { return Vector2( x+o.x, y+o.y ); }
    Vector2 operator-(Vector2 const& o) const
        { return Vector2( x-o.x, y-o.y ); }
    Vector2 operator*(T f) const
        { return Vector2( x*f, y*f ); }
    Vector2 operator/(T f) const
        { return Vector2( x/f, y/f ); }
    Vector2 norm() const
        { return Vector2( x/length(), y/length() ); }
    T operator*(Vector2 const& o) const
        { return x*o.x + y*o.y; }
    T length() const
        { return sqrt( x*x + y*y ); }
    void X( T xx )
        { x = xx; }
    void Y( T yy )
        { y = yy; }
    T X() const
        { return x; }
    T Y() const
        { return y; }
    bool operator==(Vector2 const& o) const
        { return x == o.x && y == o.y; }
};

template<typename T>
inline Vector2<T> operator*(T f, Vector2<T> v)
{ return Vector2<T>( v.X()*f, v.Y()*f ); }

typedef Vector2<float> Vector2f;
typedef Vector2<int>   Vector2i;

#endif // VECTOR2_H
