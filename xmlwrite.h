// -*-c++-*-

// fixg2sxd - a utility to convert fig to sxd format

// Copyright (C) 2003-2022 Alexander Bürger, acfb@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef _hdr_XMLWRITE_H
#define _hdr_XMLWRITE_H

#ifdef USE_MAP
#include <map>
#endif
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

// ------------------------------------------------------------------------

class Element {
public:
    Element() { }
    virtual void write(std::ostream&,int,bool) const = 0;
    virtual ~Element() { }
private:
    Element(Element const&); // disabled
    Element& operator=(Element const&); // disabled
};

inline std::ostream& operator<<( std::ostream& o, Element const* e )
{
    e->write( o, 0, true );
    return o;
}

inline std::ostream& operator<<( std::ostream& o, Element const& e )
{
    e.write( o, 0, true );
    return o;
}

// ------------------------------------------------------------------------

class TextElement : public Element
{
    std::ostringstream text;
public:
    std::ostringstream& t() { return text; }
    virtual void write(std::ostream&,int,bool) const;
    virtual ~TextElement() { }
};

// ------------------------------------------------------------------------

class Node : public Element
{
    std::string name;
    bool indentation;
    std::vector<Element*> elements;
#ifdef USE_MAP
    typedef std::map<std::string,std::ostringstream*> attributes_t;
#else
    typedef std::vector<std::pair<std::string,std::ostringstream*> > attributes_t;
#endif
    attributes_t attributes;
public:
    Node( std::string const& name );
    ~Node();

    void SetName(std::string const& aName)
        { name = aName; }

    std::ostringstream& att(std::string const& attname);
    std::ostringstream& operator[](std::string const& attname)
        { return att( attname ); }

    Node& subnode( std::string const& subname );
    TextElement& text();
    virtual void write(std::ostream&,int,bool) const;
};

#endif // _hdr_XMLWRITE_H
