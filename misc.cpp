// -*-c++-*-

// fixg2sxd - a utility to convert fig to sxd format

// Copyright (C) 2003-2022 Alexander Bürger, acfb@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "misc.h"

#include <iostream>
#include <sstream>

using namespace std;

int resolution;

/** Convert from xfig units (points per inch) to cm. */
float tr(float xfigunits)
{
    return xfigunits/(resolution/2.54f);
}

ostream& operator<<(ostream& out, ostream_tr const& o)
{
    return out << tr(o.v.X()) << ' ' << tr(o.v.Y());
}

float tr80(float xfigunits)
{
    return xfigunits/(80/2.54f);
}

int tr_p(float xfigunits)
{
    return int(1000*tr(xfigunits));
}

ostream& operator<<(ostream& out, ostream_tr_p const& o)
{
    return out << tr_p(o.v.X()) << ' ' << tr_p(o.v.Y());
}

void fail( const char* msg )
{
    ostringstream err;
    err << "Error: " << msg << endl;
    throw err.str();
}

void skip_comment( istream& figfile )
{
    figfile >> ws;
    while( figfile.peek() == '#' )
        figfile.ignore( 1<<30, '\n' );
}

int depth_max;

int depth2z(int depth)
{
    return depth_max - depth;
}
