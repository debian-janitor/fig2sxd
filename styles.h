// -*-c++-*-

// fixg2sxd - a utility to convert fig to sxd format

// Copyright (C) 2003-2022 Alexander Bürger, acfb@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef STYLES_H
#define STYLES_H

#include "xmlwrite.h"
#include <set>
#include <string>
#include <iostream>

using namespace std;

class TextStyle {
public:
    int color, font, font_flags;
    float font_size;
    // (0: Left justified 1: Center justified 2: Right justified)
    int justification;

    static string base;

    static int number;
    mutable int mynumber;
    string stylenumber() const;

    Node& write( Node& styles ) const;
    bool operator<( TextStyle const& o ) const;
    TextStyle() : justification(0), mynumber(0) { }
    void check();
};

typedef set<TextStyle> tsset;
extern tsset textstyles;

// ------------------------------------------------------------------------

class Arrow {
public:
    int   arrow_type;
    // 0 stick, 1 closed triangle, 2 closed with indented butt, 3
    // closed with pointed butt
    int   arrow_style; // (0 hollow (white fill), 1 filled)
    float arrow_width; // (Fig units)

    bool operator<(Arrow const&) const;
    istream& read( istream& );
    Node& write( Node& out ) const;
    string name() const;
};

typedef set<Arrow> arrowset;
extern arrowset arrows;

// ------------------------------------------------------------------------

class LineFillStyle {
public:
    int line_style; // (enumeration type)
    float line_thickness; // (1/80 inch)
    int pen_color; // (enumeration type, pen color)
    int fill_color; // (enumeration type, fill color)
    int pen_style; // (pen style, not used)
    int area_fill; // (enumeration type, -1 = no fill)
    float style_val; // (1/80 inch)

    int linejoin;

    arrowset::iterator forward_arrow, backward_arrow;

    static string base;

    void read( istream& figfile, int& sub_type, int& depth, bool join=false );
    void read_arrow( istream& figfile, bool forward );

    bool operator<(LineFillStyle const& o) const;

    static int number;

    std::string stylename() const;
    std::string stylename_line() const;
    std::string stylename_fill() const;
    Node& write( Node& out ) const;

    bool hasLine() const;
    bool hasFill() const;

    static float linewith1;

    LineFillStyle()
        : linejoin(-1),
          forward_arrow(arrows.end()), backward_arrow(arrows.end()),
          mynumber( 0 ), mynumber_line(0), mynumber_fill(0)
        { }

private:
    mutable int mynumber, mynumber_line, mynumber_fill;
};

typedef std::set<LineFillStyle> lfsset;
extern lfsset linefillstyles;

#endif // STYLES_H
