# fig2sxd

The fig2sxd program converts files in [XFig](http://www.xfig.org)
format into [OpenOffice.org](https://www.openoffice.org) /
[LibreOffice.org](https://www.libreoffice.org) Draw format.

### Status

Conversion works basically good, but some things are not mapped exactly:

 - splines are quite similar in shape, but definitively different
 - hollow arrows are mapped to filled ones
 - hatches are different
 - and more

Feedback is very welcome. To give feedback, please create an issue.

### Platforms

The program has been run both under Linux&reg; and MS Windows&reg;.

As it depends only on standard C++ and [zlib](https://zlib.net) it
should be easy to compile on other systems.

### License and Copyright

fig2sxd is Copyright 2003-2018 Alexander Bürger, acfb (at) users.sourceforge.net.

fig2sxd is licensed under GPL v2. Please see [COPYING](file:COPYING)
for the full the license text.
