// -*-c++-*-

// fixg2sxd - a utility to convert fig to sxd format

// Copyright (C) 2003-2022 Alexander Bürger, acfb@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "styles.h"
#include "keep_range.h"

// TODO: hollow arrows

arrowset arrows;

istream& Arrow::read( istream& figfile )
{
    // these two are ignored
    float arrow_thickness; // (1/80 inch)
    float arrow_height; // (Fig units)
    figfile >> arrow_type >> arrow_style
            >> arrow_thickness >> arrow_width >> arrow_height;

    // check that all values are in the specified ranges
    keep_range( arrow_type,      "arrow_type",      0, 3 );
    keep_range( arrow_style,     "arrow_style",     0, 1 );
    keep_range( arrow_thickness, "arrow_thickness", 0.f );
    keep_range( arrow_width,     "arrow_width",     0.f );
    keep_range( arrow_height,    "arrow_heigth",    0.f );

    return figfile;
}

string Arrow::name() const
{
    const char *names[4] = { "Stick", "ClosedTriangle",
                             "IndentedButt", "PointedButt" };
    const char styles[2] = { 'O', 'F' };
    return string(names[arrow_type]) + styles[arrow_style];
}

Node& Arrow::write( Node& out ) const
{
    const int boxheight[4] = { 2000, 1000, 1000, 1250 };
    const char* paths[4] = {
        "M0 2000 L750 0 1500 2000 1250 2000 750 500 250 2000Z",
        "M0 1000 L750 0 1500 1000Z",
        "M0 1000 L750 0 1500 1000 750 750Z",
        "M0 1000 L750 0 1500 1000 750 1250Z"
    };
    Node& arrow = out.subnode("draw:marker");
    arrow["draw:name"] << name();

    // OOo reads arrows only if the svg:viewBox and svg:d attributes
    // are in this order
    arrow["svg:viewBox"] << "0 0 1500 " << boxheight[arrow_type];
    arrow["svg:d"] << paths[ arrow_type ];

    return out;
}

#define cmp(x) if( x < o.x ) return true; if( x > o.x ) return false
bool Arrow::operator<(Arrow const& o) const
{
    cmp( arrow_type );
    cmp( arrow_style );
    // cmp( arrow_width ); is compared at LineFillStyle
    return false;
}

