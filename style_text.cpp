// -*-c++-*-

// fixg2sxd - a utility to convert fig to sxd format

// Copyright (C) 2003-2022 Alexander Bürger, acfb@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "styles.h"

#include "xmlwrite.h"
#include "keep_range.h"
#include "colors.h"
#include <sstream>
#include <iomanip>

tsset textstyles;

string TextStyle::base = "Fig2SxdText";
int TextStyle::number = 10;

void TextStyle::check()
{
    keep_range(justification, "justification", 0, 2 );
    keep_range(color, "color", -1, 32+512-1 );
}

string TextStyle::stylenumber() const
{
    if( mynumber==0 )
        mynumber = ++number;
    ostringstream s;
    s << base << mynumber;
    return s.str();
}

Node& TextStyle::write( Node& out ) const
{
    const char* fontname = "Helvetica";
    bool bold = false;
    bool italic = false;
    if( font_flags & 0x4 ) { // Postscript fonts
        //cout << "ps fonts" << endl;
        if( font == -1 ) {
            // default
        } else if( font >= 0 && font <= 31 ) {
            const char* fontnames[] = {
                "Times New Roman", "AvantGarde", "Bookman", "Courier",
                "Helvetica",
                "Helvetica", // narrow by using "narrow" text style
                "New Century Schoolbook", "Palatino" };
            fontname = fontnames[font/4];
            bold   = ((font & 2) != 0);
            italic = ((font & 1) != 0);
        } else if( font>=32 && font<=34 ) {
            const char* fontnames[] = { "Standard Symbols L", "Zapf Chancery",
                                        "Zapf Dingbats" };
            fontname = fontnames[font-32];
        }
    } else { // LaTeX fonts
        if( font>=0 && font<=5 ) {
            const char* fontnames[] = {
                "Helvetica", "Times New Roman", "Times New Roman",
                "Times New Roman", "Courier" };
            fontname = fontnames[font];
            bold   = (font == 2);
            italic = (font == 3);
        }
    }
    Node& style = out.subnode("style:style");
    style["style:name"] << stylenumber();
    style["style:family"] << "graphics"; // paragraph
    style["style:class"] << "text";
    style["style:parent-style-name"] << base;
    Node& prop = style.subnode("style:properties");
    prop["fo:font-family"] << fontname;
    prop["draw:textarea-vertical-align"] << "middle";
    prop["fo:font-weight"] << (bold ? "bold" : "normal" );
    prop["fo:font-style"] << (italic ? "italic" : "normal" );
    prop["fo:color"] << colorstring(color);
    prop["fo:font-size"] << (font_size*(72.0/80.0)) << "pt";
    const char* align[3] = { "left", "center", "right" };
    prop["draw:textarea-horizontal-align"] << align[justification];
    return out;
}

#define cmp(x) if( x < o.x ) return true; if( x > o.x ) return false
bool TextStyle::operator<(TextStyle const& o) const
{
    cmp( color );
    cmp( font );
    cmp( font_flags );
    cmp( font_size );
    cmp( justification );
    return false;
}

