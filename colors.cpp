// -*-c++-*-

// fixg2sxd - a utility to convert fig to sxd format

// Copyright (C) 2003-2022 Alexander Bürger, acfb@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "colors.h"

#include "misc.h"

#include <cstdlib>
#include <cstring>
#include <iomanip>
#include <string>

using namespace std;

static unsigned int hexcolors_with_minus_1[545];

unsigned int *hexcolor = 0;

void read_color( istream& figfile )
{
    int color_number;
    string hex; // XXX bad if malformed hex value!
    figfile >> color_number >> hex;
    if( color_number<32 || color_number>543 )
        fail( "Color number out of range." );
    if( hex.size() >= 2 )
        hexcolor[color_number] = strtol( hex.c_str()+1, 0, 16 );
    else
        fail( "Color name too short." );
}

void initcolors()
{
    hexcolor = hexcolors_with_minus_1 + 1;

    hexcolor[-1] = 0x000000; // Black
    hexcolor[0]  = 0x000000; // Black
    hexcolor[1]  = 0x0000ff; // Blue
    hexcolor[2]  = 0x00ff00; // Green
    hexcolor[3]  = 0x00ffff; // Cyan
    hexcolor[4]  = 0xff0000; // Red
    hexcolor[5]  = 0xff00ff; // Magenta
    hexcolor[6]  = 0xffff00; // Yellow
    hexcolor[7]  = 0xffffff; // White
    hexcolor[8]  = 0x000090; // four shades of blue (dark to lighter)
    hexcolor[9]  = 0x0000b0;
    hexcolor[10] = 0x0000d0;
    hexcolor[11] = 0x87ceff;
    hexcolor[12] = 0x009000; // three shades of green (dark to lighter)
    hexcolor[13] = 0x00b000;
    hexcolor[14] = 0x00d000;
    hexcolor[15] = 0x009090; // three shades of cyan (dark to lighter)
    hexcolor[16] = 0x00b0b0;
    hexcolor[17] = 0x00d0d0;
    hexcolor[18] = 0x900000; // three shades of red (dark to lighter)
    hexcolor[19] = 0xb00000;
    hexcolor[20] = 0xd00000;
    hexcolor[21] = 0x900090; // three shades of magenta (dark to lighter)
    hexcolor[22] = 0xb000b0;
    hexcolor[23] = 0xd000d0;
    hexcolor[24] = 0x803000; // three shades of brown (dark to lighter)
    hexcolor[25] = 0xa04000;
    hexcolor[26] = 0xc06000;
    hexcolor[27] = 0xff8080; // four shades of pink (dark to lighter)
    hexcolor[28] = 0xffa0a0;
    hexcolor[29] = 0xffc0c0;
    hexcolor[30] = 0xffe0e0;
    hexcolor[31] = 0xffd700; // Gold
    int i;
    for( i=32; i<544; ++i )
        hexcolor[i] = 0;
}

std::string colorstring(int index)
{
    if( index < -1 || index > 543 )
        // -1 is a valid index in hexcolor, it points to the second
        // -entry in hexcolors_with_minus_1
        index = -1;
    return color2string( hexcolor[index] );
}

string color2string(unsigned int color)
{
    static const char map[17] = "0123456789ABCDEF";
    char tmp[8] = "#123456";
    for( int i=0; i<6; ++i ) {
        tmp[6-i] = map[ color&0xf ];
        color >>= 4;
    }
    return string(tmp);
}
