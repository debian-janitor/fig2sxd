// -*-c++-*-

// fixg2sxd - a utility to convert fig to sxd format

// Copyright (C) 2003-2022 Alexander Bürger, acfb@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "keep_range.h"

#include <sstream>
#include <iostream>
#include <cassert>
using namespace std;

bool out_of_range_error=true;

template<typename T>
void keep_range( T& value, const char* name, const T mini, const T maxi )
{
    assert( mini <= maxi );
    if( value < mini || value > maxi ) {
        if( out_of_range_error ) {
            ostringstream o;
            o << "Error: " << name << '=' << value << " out of range ["
              << mini << ',' << maxi << "]";
            throw o.str();
        } else {
            cerr << "Warning: " << name << " was " << value
                 << " which is out of range [" << mini << ','
                 << maxi << "]; set to " << mini << '.' << endl;
            value = mini;
        }
    }
}

template
void keep_range<float>( float& v, const char* name, const float l, const float u );

template
void keep_range<int>( int& v, const char* name, const int l, const int u );
